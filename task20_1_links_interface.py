import shelve
from task20_1_links_methods import ShortLinks


class Interface:
    def __init__(self, file_name='data/database_links.dat'):
        self.file_name = file_name
        with shelve.open(file_name) as db:
            self.links_data = ShortLinks()
            if 'links_data' in db.keys():
                # print(db['links_data'])
                self.links_data.update(db['links_data'])

    def run_input(self):
        while True:
            print("\nSelect action: ")
            action = input("press 'Enter' - add new link\n"
                           "g - get link by shortener\n"
                           "q - for quit\n")
            if action == 'g':
                short_link = input("input shortener: ")
                self.links_data.get_link(short_link)
                print('*'*20)
            elif action == 'q':
                with shelve.open(self.file_name) as db:
                    if 'links_data' in db.keys():
                        temp = db['links_data']
                        temp.update(self.links_data)
                        db['links_data'] = temp
                    else:
                        db['links_data'] = self.links_data
                break
            else:
                link = input("input link: ")
                short_link = input("input shortener: ")
                print('*' * 20)
                self.links_data.add_link(link, short_link)


if __name__ == "__main__":
    data_interface = Interface()
    data_interface.run_input()