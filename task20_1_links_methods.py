class ShortLinks(dict):

    def add_link(self, link, short_link):
        short_link = short_link.lower()
        if short_link in self.keys():
            print("The entry will not be added: this shortener is already in use!")
        else:
            self[short_link] = link

    def get_link(self, short_link):
        short_link = short_link.lower()
        link = self.get(short_link)
        if not link:
            print("No link found for this link shortener!")
        else:
            print(f"Link: {link}")
            return link
