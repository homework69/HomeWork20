def prime_sequence():
    n = 1
    while True:
        n += 1
        is_prime = True
        for i in range(2, n // 2 + 1):
            if n % i == 0:
                is_prime = False
                break
        if is_prime:
            yield n


def prime_generator(limit):
    generator = prime_sequence()
    for _ in range(limit):
        yield next(generator)


def nth_prime(number):
    result = None
    for current in prime_generator(number):
        result = current
    return result


if __name__ == "__main__":
    print(nth_prime(1))
    print(nth_prime(3))
    print(nth_prime(5))
    print(nth_prime(8))
    print(nth_prime(21))
