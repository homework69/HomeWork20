import task20_3_prime as prime

if __name__ == "__main__":
    print(prime.nth_prime(23))
    print(prime.nth_prime(50))
    print(prime.nth_prime(100))

    from task20_3_prime import nth_prime as get_prime
    print(get_prime(13))
    print(get_prime(15))
    print(get_prime(38))

